package com.example.demo.web;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.open;

public class MainPageTest {

	private final MainPage mainPage = new MainPage();

	@BeforeClass
	public static void setUpAllure() {
		SelenideLogger.addListener("allure", new AllureSelenide());
	}

	@BeforeMethod
	public void setUp() {
		Configuration.startMaximized = true;
		open("https://metro.zakaz.ua/uk/");
	}

	@Test
	public void search(){
		mainPage.openPage();
		mainPage.checkingNumberOfProducts("банан", 5);
	}
}
