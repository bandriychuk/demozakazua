package com.example.demo.api;

import com.example.demo.api.apiConfig.TestConfig;
import io.qameta.allure.Step;
import io.restassured.response.Response;
import org.testng.annotations.Test;

import static com.example.demo.api.helpers.StoreHelper.*;
import static io.restassured.RestAssured.given;
import static org.testng.Assert.assertTrue;

public class NumberProductsTest extends TestConfig {

	@Test
	@Step("Сhecking the quantity of products in stores")
	public void testNumberProductsInStore() {
		Response getStores = given().log().all()
				.when()
				.headers(X_Chain, X_ChainValue)
				.queryParam(queryParamCoords, queryParamCoordsValueForKyiv)
				.queryParam(queryParamRegionId, queryParamRegionIdValueForKyiv)
				.contentType(ContentTypeApplicationJson)
				.basePath(GET_STORE)
				.when().get().then().spec(responseSpecificationForGet).extract().response();
		String id = getStores.jsonPath().getString("id");
		String[] subStr;
		String delimeter = ",";
		subStr = id.split(delimeter);
		for (int i = 0; i < id.length(); i++) {
			String idStore = subStr[i];
			String store = idStore.substring(1, 9);
			Response getProductsNumber = given().log().all()
					.when()
					.headers(StoreId, i)
					.headers(X_Chain, X_ChainValue)
					.contentType(ContentTypeApplicationJson)
					.basePath(GET_STORE_ID_PRODUCTS_COUNT(store))
					.when().get().then().spec(responseSpecificationForGet).extract().response();
			int total = getProductsNumber.jsonPath().getInt("total");
			System.out.println(total);
			assertTrue(total >= 10000, "In store with id " + store + " Products less then 10000");
		}
	}
}



