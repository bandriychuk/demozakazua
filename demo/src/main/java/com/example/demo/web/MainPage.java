package com.example.demo.web;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import static com.codeborne.selenide.CollectionCondition.sizeGreaterThanOrEqual;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$x;

public class MainPage {

	public SelenideElement searchButton = $(By.xpath("//input[@data-marker='Search Input']"));
	public SelenideElement loupe = $(By.xpath("//div[@data-testid='loupe']"));
	public String listWithProducts = "//*[@id='PageWrapBody_desktopMode']//div[contains(@class, 'jsx-33926795 ProductsBox__listItem')]";


	@Step("Open home page")
	public void openPage() {
		$(searchButton).click();
	}

	@Step("Open results page")
	public void checkingNumberOfProducts(String productName, int numberProducts) {
		$(searchButton).sendKeys(productName);
		$(loupe).click();
		$$x(listWithProducts).shouldHave(sizeGreaterThanOrEqual(numberProducts));
	}
}
