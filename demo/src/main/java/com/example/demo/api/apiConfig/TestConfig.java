package com.example.demo.api.apiConfig;

import io.restassured.RestAssured;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.specification.ResponseSpecification;
import org.testng.annotations.BeforeClass;


public class TestConfig {

    @BeforeClass
    public void setUp() {
        RestAssured.baseURI = Routes.URL;
    }

    public static ResponseSpecification responseSpecificationForGet = new ResponseSpecBuilder()
            .expectStatusCode(200)
            .build();
}
