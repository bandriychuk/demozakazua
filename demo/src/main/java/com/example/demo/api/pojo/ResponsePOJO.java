package com.example.demo.api.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponsePOJO {

    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("retail_chain")
    @Expose
    private String retailChain;
    @SerializedName("region_id")
    @Expose
    private String regionId;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("phones")
    @Expose
    private List<String> phones = null;
    @SerializedName("address")
    @Expose
    private Address address;
    @SerializedName("coords")
    @Expose
    private String coords;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("delivery_types")
    @Expose
    private List<String> deliveryTypes = null;
    @SerializedName("payment_types")
    @Expose
    private List<String> paymentTypes = null;
    @SerializedName("opening_hours")
    @Expose
    private OpeningHours openingHours;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRetailChain() {
        return retailChain;
    }

    public void setRetailChain(String retailChain) {
        this.retailChain = retailChain;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public List<String> getPhones() {
        return phones;
    }

    public void setPhones(List<String> phones) {
        this.phones = phones;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getCoords() {
        return coords;
    }

    public void setCoords(String coords) {
        this.coords = coords;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getDeliveryTypes() {
        return deliveryTypes;
    }

    public void setDeliveryTypes(List<String> deliveryTypes) {
        this.deliveryTypes = deliveryTypes;
    }

    public List<String> getPaymentTypes() {
        return paymentTypes;
    }

    public void setPaymentTypes(List<String> paymentTypes) {
        this.paymentTypes = paymentTypes;
    }

    public OpeningHours getOpeningHours() {
        return openingHours;
    }

    public void setOpeningHours(OpeningHours openingHours) {
        this.openingHours = openingHours;
    }

}
