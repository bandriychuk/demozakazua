package com.example.demo.api.helpers;

public class StoreHelper {
	public static String GET_STORE = "/stores/";
	public static String GET_STORE_ID = "/stores/";

	public static String GET_STORE_ID_PRODUCTS_COUNT (String id){
		String route = "/stores/"+id+"/products_count/";
		return route;
	}

	public static String StoreId = "store_id";

	public static String X_Chain = "X-Chain";
	public static String X_ChainValue = "*";
	public static String queryParamCoords = "coords";
	public static String queryParamCoordsValueForKyiv = "50.421811,30.610563";

	public static String queryParamRegionId = "region_id";
	public static String queryParamRegionIdValueForKyiv = "kiev";
	public static String ContentTypeApplicationJson = "application/json";
}
